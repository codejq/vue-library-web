import { createApp } from 'vue'
import App from './App.vue'
import route from './router'
import 'bootstrap/dist/css/bootstrap.css';
import '@fortawesome/fontawesome-free/css/all.css'

createApp(App)
.use(route)
.mount('#app')
