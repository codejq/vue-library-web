import { createRouter, createWebHistory } from 'vue-router'
import PaginaPrincipal from './components/Generic/Home.vue'
import TablaCategorias from './components/Genres/TableDataGenre.vue'
import UserTable from './components/Users/TableDataUsers.vue'
import CountriesTable from './components/Countries/TableDataCountries.vue'
import AuthorsTable from './components/Authors/TableDataAuthors.vue'
import BooksTable from './components/Books/TableDataBooks.vue'
import LoansTable from './components/Loans/TableDataLoans.vue'
// Genres
import NewGenre from './components/Genres/NewGenre.vue'
import DeleteGenre from './components/Genres/DeleteGenre.vue'
import EditGenre from './components/Genres/EditGenre.vue'
// Authors
import NewAuthor from './components/Authors/NewAuthor.vue'
import DeleteAuthor from './components/Authors/DeleteAuthor.vue'
import EditAuthor from './components/Authors/EditAuthor.vue'
// Books
import NewBook from './components/Books/NewBook.vue'
import DeleteBook from './components/Books/DeleteBook.vue'
import EditBook from './components/Books/EditBook.vue'
// Countries
import NewCountry from './components/Countries/NewCountry.vue'
import DeleteCountry from './components/Countries/DeleteCountry.vue'
import EditCountry from './components/Countries/EditCountry.vue'
// Loans
import NewLoan from './components/Loans/NewLoan.vue'
import DeleteLoan from './components/Loans/DeleteLoan.vue'
import EditLoan from './components/Loans/EditLoan.vue'
// Users
import NewUser from './components/Users/NewUser.vue'
import DeleteUser from './components/Users/DeleteUser.vue'
import EditUser from './components/Users/EditUser.vue'

const routes = [
    { path: '/', component: PaginaPrincipal },
    { path: '/genres', component: TablaCategorias },
    { path: '/users', component: UserTable },
    { path: '/authors', component: AuthorsTable },
    { path: '/books', component: BooksTable },
    { path: '/loans', component: LoansTable },
    { path: '/countries', component: CountriesTable },
    // Genres
    { path: '/cgenre', component: NewGenre },
    { path: '/ugenre/:id', component: EditGenre, props: true },
    { path: '/dgenre/:id', component: DeleteGenre, props: true },
    // Authors
    { path: '/cauthor', component: NewAuthor },
    { path: '/uauthor/:id', component: EditAuthor, props: true },
    { path: '/dauthor/:id', component: DeleteAuthor, props: true },
    // Loans
    { path: '/cloan', component: NewLoan },
    { path: '/uloan/:id', component: EditLoan, props: true },
    { path: '/dloan/:id', component: DeleteLoan, props: true },
    // Users
    { path: '/cuser', component: NewUser },
    { path: '/uuser/:id', component: EditUser, props: true },
    { path: '/duser/:id', component: DeleteUser, props: true },
    // Books
    { path: '/cbook', component: NewBook },
    { path: '/ubook/:id', component: EditBook, props: true },
    { path: '/dbook/:id', component: DeleteBook, props: true },
    // Countries
    { path: '/ccountry', component: NewCountry },
    { path: '/ucountry/:id', component: EditCountry, props: true },
    { path: '/dcountry/:id', component: DeleteCountry, props: true },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
