const EMPTY = "";
const DEFAULT_DATE = "1900-01-01";

export const USER_ITEM =
{
  "id": 0,
  "firstName": EMPTY,
  "lastName": EMPTY,
  "address": EMPTY,
  "email": EMPTY,
  "phoneNumber": EMPTY,
  "fullName": EMPTY
};

export const COUNTRY_ITEM =
{
  "id": 0,
  "name": EMPTY,
  "areaCode": EMPTY,
  "isoCode": EMPTY
};


export const GENRE_ITEM =
{
  "id": 0,
  "name": EMPTY
};

export const AUTHOR_ITEM = {
  "id": 0,
  "firstName": EMPTY,
  "lastName": EMPTY,
  "birthday": DEFAULT_DATE,
  "country": COUNTRY_ITEM,
  "fullName": EMPTY
};

export const BOOK_ITEM =
{
  "id": 0,
  "title": EMPTY,
  "publicationYear": 0,
  "author": AUTHOR_ITEM,
  "fullName": EMPTY,
  "genre": GENRE_ITEM
};

export const LOAN_ITEM =
{
  "id": 0,
  "loanDate": DEFAULT_DATE,
  "returnDate": DEFAULT_DATE,
  "user": USER_ITEM,
  "book": BOOK_ITEM
};


